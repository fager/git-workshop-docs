= Konfiguration

Der git-Client kann in verschiedenen Bereichen konfiguiert werden, um ihn an
die eigenen Bedürfnisse anzupassen.

== Konfigdateien

Für `git` gibt es zwei wichtige Konfigurationsdateien.

Die Datei `.git/config` existiert in jedem Git-Repository und speichert die
Konfigurationen, die nur für dieses Repository gelten sollen.

In `~/.gitconfig` werden die Einstellungen für den aktuellen Benutzer
gespeichert.

Die Einstellungen im Git-Repository überschreiben die jeweiligen
Einstellungen in der Benutzerweiten Konfiguration. Dadurch ist es z.B.
möglich für einzelne Repositories die Commits mit einer anderen Mailadresse
durchzuführen.

== Bearbeiten der Konfiguration mit "git config"

Der Befehl `git config` ermöglicht es die Einstellungen einzusehen und
anzupassen.

Die aktuell gültige Konfiguration kann wir folgt ausgegeben werden:

[source,bash]
----
$ git config -l
user.name=Max Mustermann
user.email=max@example.de
----

Beim Bearbeiten der Konfiguration kann mit dem Schalten `--global` angegeben
werden, dass die Einstellung in der Datei `~/.gitconfig` gespeichert werden
soll. Ansonsten wird die Einstellung in `.git/config` des aktuellen
Repository gespeichert.

[source,bash]
----
$ git config --global user.name "Max Mustermann"
$ git config --global user.email "max@example.de"
----

Die Konfiguration von `user.name` und `user.email` sollte direkt nach der
Installation einmal global durchgeführt werden. ansonsten generiert git beim
Commit aus dem Benutzernamen und dem Hostnamen des Systems eine Mailadresse
und verwendet den Anzeigenamen des aktuellen Benutzers als Name für den
Commit.

== Nützliche Konfigurationen

=== Alias `tree` für die Anzeige der Logs

Zur verkürzten Ansicht des Änderungsbaums kann ein Alias definiert werden,
der die Baumstruktur anzeigt.

[source,bash]
----
$ git config --global \
    alias.tree "log --graph --decorate --pretty=oneline --abbrev-commit"
----

Aufruf mit:
[source,bash]
----
$ git tree
* 3939544 (HEAD -> master) file_master: End
*   511968c feature-3 merged to master
|\
| * 332b62c (feature-3) file_3: 2. Zeile
| * 04ae2c9 (tag: preview_feature-3) file_3: 1. Zeile
* | 13dd5b4 file_master: 8th Line
* | eac1226 file_master: 7th Line
* |   860ab4d feature-2 merged to master
|\ \
| * | 56e0523 (feature-2) file_2: anden ile
| * | 124763e file_2: første ile
| |/
* | 0c8d905 file_master: 6th line
* | d076c7c file_master: 5th Line
|/
* 9bcee9a file_master: 4th Line
*   6207f49 feature-1 merged into master
|\
| * 4f6828b (feature-1) file_1: Dritte Zeile
| * 6d1aa54 file_1: Zweite Zeile
| * 8fe6b78 file_1: Erste Zeile
* | 4c78cc1 file_master: Third Line
|/
* c214707 file_master: Second Line
* ed83b4d file_master: First Line
* 6b8b7a8 Repo created
----

=== Editor für die Commit-Messages einstellen

`git` verwendet normalerweise den Default-Editor, der entweder systemweit
oder für einen Benutzer konfiguriert ist. Der Editor lässt sich aber auch
in der git-config einstellen.

[source,bash]
----
$ git config --global core.editor vi
----


=== Weitere Informationen zur Konfiguration und den möglichen Einstellungen

Die Manpage `git-config(1)` enthält weitere Informationen zur Konfig und 
listet alle möglichen Einstellungen mit Beschreibungen auf.
 